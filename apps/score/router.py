from fastapi import APIRouter, Request, Depends, FastAPI, status
from sqlalchemy.orm.session import Session
from apps.score.models import Score
from .schemas import DeleteScoreSchema, ScoreSchema, CreateScoreSchema
from db.database import get_db


router : APIRouter = APIRouter()

@router.get("/score", response_model=ScoreSchema)
async def get_score_by_user_id(user_id: int, db: Session = Depends(get_db)):
    return db.query(Score).filter(Score.user_id == user_id)


@router.post("/", status_code=status.HTTP_201_CREATED)
async def add_score(score: CreateScoreSchema, db : Session = Depends(get_db)):
    db_score = Score(user_id = score.user_id, score = score.score)
    db.add(db_score)
    db.commit()
    db.refresh(db_score)
    return db_score

@router.delete("/", status_code=status.HTTP_204_NO_CONTENT)
async def delete_score(user_id : DeleteScoreSchema, db : Session = Depends(get_db)):
    db_score = Score(user_id = user_id.user_id)
    db.delete(db_score)
    db.commit()
    db.refresh(db_score)
    return db_score 

