from sqlalchemy import Integer, ForeignKey, DateTime, Column
from db.database import Base
from datetime import datetime


class Score(Base):
    __tablename__ = "score"
    
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    score = Column(Integer)
    date_of_score = Column(DateTime, default=datetime.utcnow)
