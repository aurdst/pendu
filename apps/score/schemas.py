from apps.score.models import Score
from datetime import datetime
from apps.user.models import UserModel
from typing import List, Optional
from pydantic import BaseModel

class ScoreSchema(BaseModel):
    score: int
    date_of_score: datetime
    user_id: int

class CreateScoreSchema(BaseModel):
    score : int 
    user_id : int

    class Config:
        arbitrary_types_allowed = True 

class DeleteScoreSchema(BaseModel):
    user_id : int
