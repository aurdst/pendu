from sqlalchemy import String, Integer, Column, DateTime
from sqlalchemy.orm import relationship
from db.database import Base
from datetime import datetime


class UserModel(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True)
    date_added = Column(DateTime, default=datetime.utcnow)

    # scores = relationship("Score", back_populates="scores")
