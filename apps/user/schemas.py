from pydantic import BaseModel
from datetime import datetime
from typing import List


class UserSchema(BaseModel):
    id: int
    username: str
    date_added: datetime
    # scores: List[int]

    class Config:
        orm_mode = True 


class UserCreateSchema(BaseModel):
    username: str

class DeleteUserSchema(BaseModel):
    id: int

    class Config:
        orm_mode = True 