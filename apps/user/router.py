from fastapi import APIRouter, Request, Depends, FastAPI, status, HTTPException
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.functions import user
from starlette.status import HTTP_204_NO_CONTENT
from apps.user.models import UserModel
from .schemas import DeleteUserSchema, UserSchema, UserCreateSchema
from db.database import get_db
from typing import List

router = APIRouter = APIRouter()

@router.post("/", status_code=status.HTTP_201_CREATED)
async def create_user(user : UserCreateSchema, db : Session = Depends(get_db)):
    db_user = UserModel(username = user.username)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

@router.get("/", response_model=List[UserSchema])
async def  get_all_users(db : Session = Depends(get_db)):
    data = db.query(UserModel).all()
    for d in data:
        print(d.username, d.id, d.date_added)
    return data

@router.delete("/", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user_by_id(user_id: DeleteUserSchema, db : Session = Depends(get_db)):
    db_user = db.query(UserModel).filter(UserModel.id == user_id.id).first()
    try:
        db.delete(db_user)
        db.commit()
    except:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


