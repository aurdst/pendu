from fastapi import FastAPI
from apps.score.router import router as score_router
from apps.user.router import router as user_router
import uvicorn


app: FastAPI = FastAPI(debug=True)


app.include_router(score_router, tags=["Score"], prefix="/api/v1/score")
app.include_router(user_router, tags=['User'], prefix = '/api/v1/user')


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
